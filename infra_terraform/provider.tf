provider "aws" {}
locals {
  common_tags = {
    BillingCode = var.billing_code_tag
    Environment = var.environment_tag
  }
 }
 resource "random_integer" "rand" {
  min = 10000
  max = 99999
 }