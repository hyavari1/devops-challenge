# Microservice INSTANCES #

resource "aws_key_pair" "hesammac" {
  key_name   = var.key_name
  public_key = var.public_key
 }

resource "aws_instance" "microservice" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.medium"
  subnet_id              = aws_subnet.microservice.id
  vpc_security_group_ids = [aws_security_group.microservice.id]
  key_name               = var.key_name
  tags                   = merge(local.common_tags, { Name = "${var.environment_tag}-microservice" })
 }