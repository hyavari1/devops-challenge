
resource "aws_subnet" "loadbalancer" {
  cidr_block              = var.loadbalancer_address_space
  vpc_id                  = data.aws_vpc.selected.id
  map_public_ip_on_launch = "true"
  availability_zone       = data.aws_availability_zones.available.names[1]
  tags                    = merge(local.common_tags, { Name = "${var.environment_tag}-loadbalancer" })

 }

resource "aws_subnet" "microservice" {
  cidr_block              = var.microservice_address_space
  vpc_id                  = data.aws_vpc.selected.id
  map_public_ip_on_launch = "true"
  availability_zone       = data.aws_availability_zones.available.names[1]
  tags                    = merge(local.common_tags, { Name = "${var.environment_tag}-microservice" })

 }
resource "aws_subnet" "database" {
  cidr_block              = var.database_address_space
  vpc_id                  = data.aws_vpc.selected.id
  map_public_ip_on_launch = "true"
  availability_zone       = data.aws_availability_zones.available.names[1]
  tags                    = merge(local.common_tags, { Name = "${var.environment_tag}-database" })

 }
# ROUTING #
resource "aws_route_table" "rtb" {
  vpc_id = data.aws_vpc.selected.id

  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = var.igw_id
  }
  tags = merge(local.common_tags, { Name = "${var.environment_tag}-rtb" })
 }

resource "aws_route_table_association" "rta-loadbalancer" {
  subnet_id      = aws_subnet.loadbalancer.id
  route_table_id = aws_route_table.rtb.id
 }
resource "aws_route_table_association" "rta-microservice" {
  subnet_id      = aws_subnet.microservice.id
  route_table_id = aws_route_table.rtb.id
 }
resource "aws_route_table_association" "rta-database" {
  subnet_id      = aws_subnet.database.id
  route_table_id = aws_route_table.rtb.id
 }