resource "aws_instance" "managment" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.microservice.id
  vpc_security_group_ids = [aws_security_group.microservice.id]
  key_name               = var.key_name
  tags                   = merge(local.common_tags, { Name = "${var.environment_tag}-managment" })
 }