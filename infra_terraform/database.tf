resource "aws_instance" "database" {
  ami                    = data.aws_ami.ubuntu.id
  instance_type          = "t2.micro"
  subnet_id              = aws_subnet.database.id
  vpc_security_group_ids = [aws_security_group.database.id]
  key_name               = var.key_name
  tags                   = merge(local.common_tags, { Name = "${var.environment_tag}-database" })
 }