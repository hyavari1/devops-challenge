output "aws_microservice_public_ip" {
  value = aws_instance.microservice.public_ip
}
output "aws_loadbalancer_public_ip" {
  value = aws_instance.loadbalancer.public_ip
}
output "aws_database_public_ip" {
  value = aws_instance.database.public_ip
}
output "aws_managment_public_ip" {
  value = aws_instance.managment.public_ip
}
output "aws_loadbalancer_privite_ip" {
  value = aws_instance.loadbalancer.private_ip
}
output "aws_microservice_privite_ip" {
  value = aws_instance.microservice.private_ip
}
output "aws_managment_privite_ip" {
  value = aws_instance.managment.private_ip
}
output "aws_database_privite_ip" {
  value = aws_instance.database.private_ip
}