variable "private_key_path" {}
variable "public_key" {}

variable "key_name" {
  default = "hesammac"
}
variable "bucket_name_prefix" {
  default = "jumiabucket"
}
variable "billing_code_tag" {
  default = "jumia-phone-validator"
}
variable "environment_tag" {
   default = "jumiaenv"
}
variable "region" {
  default = "us-east-1"
}
variable "vpc_id" {
  default = "vpc-0b6aefdfa73fce547"
}
data "aws_vpc" "selected" {
  id = var.vpc_id
}
variable "igw_id" {
  default = "igw-075acbab438f17c2f"
}

variable "loadbalancer_address_space" {
  default = "10.0.10.0/24"
}
variable "microservice_address_space" {
  default = "10.0.20.0/24"
}
variable "database_address_space" {
  default = "10.0.30.0/24"
}