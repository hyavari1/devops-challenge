
provider "aws" {
  region = "us-east-1"
}
variable "private_key_path" {}
variable "public_key" {}
variable "key_name" {}