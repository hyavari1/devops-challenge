resource "kubernetes_deployment" "nginx" {
  metadata {
    name = "test-app"
    labels = {
      App = "testdeploy"
    }
  }

  spec {
    replicas = 3
    selector {
      match_labels = {
        App = "testdeploy"
      }
    }
    template {
      metadata {
        labels = {
          App = "testdeploy"
        }
      }
      spec {
        container {
          image = "nginx:1.7.8"
          name  = "jumiaweb"

          port {
            container_port = 80
          }

          resources {
            limits = {
              cpu    = "0.5"
              memory = "512Mi"
            }
            requests = {
              cpu    = "250m"
              memory = "50Mi"
            }
          }
        }
      }
    }
  }
}
