#!/bin/bash -ex
 
##### ssh port change #####

sudo perl -pi -e 's/^#?Port 22$/Port 1337/' /etc/ssh/sshd_config
sudo service sshd restart

sudo apt-get update

##### Config firewall #####

sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow 1337/tcp
sudo ufw allow 5432/tcp
sudo ufw allow from 10.0.20.0/24
sudo ufw enable
sudo ufw reload

##### Install potgres Database #####

sudo apt install wget ca-certificates -y
sudo apt-get install postgresql postgresql-contrib net-tools -y
sudo systemctl enable postgresql.service
sudo systemctl start postgresql.service

##### config and creat jumia_phone_validator Database #####

sudo -u postgres psql -c "CREATE DATABASE jumia_phone_validator;"
sudo -u postgres psql -c "CREATE USER jumia WITH ENCRYPTED PASSWORD 'jumi@test1nterview';"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON DATABASE jumia_phone_validator TO jumia;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON TABLE customer TO jumia;"
sudo -u postgres psql -c "GRANT CONNECT ON DATABASE jumia_phone_validator TO jumia;"
sudo -u postgres psql -c "GRANT SELECT, INSERT, UPDATE, DELETE ON ALL TABLES IN SCHEMA public TO jumia;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON ALL TABLES IN SCHEMA public TO jumia;"
sudo -u postgres psql -c "GRANT ALL PRIVILEGES ON ALL SEQUENCES IN SCHEMA public TO jumia;"
sudo -u postgres psql -c "ALTER USER jumia CREATEDB;"
sudo -u postgres psql -c "ALTER USER jumia WITH SUPERUSER;"
##### Insert sample.sql #####

sudo -u postgres psql jumia_phone_validator < sample.sql

sudo service sshd restart

