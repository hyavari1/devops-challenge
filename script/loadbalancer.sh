#!/bin/bash -ex
sudo apt-get update

##### ssh port change #####

sudo perl -pi -e 's/^#?Port 22$/Port 1337/' /etc/ssh/sshd_config

##### Config firewall #####

sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow 1337/tcp
sudo ufw allow 80/tcp
sudo ufw allow 443/tcp
sudo ufw allow 22/tcp
sudo ufw allow from 10.0.0.0/16
sudo ufw enable
sudo ufw reload

##### Install nginx webserver #####

sudo apt-get -y install nginx
sudo systemctl enable nginx
sudo systemctl start nginx

sudo service sshd restart