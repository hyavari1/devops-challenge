#!/bin/bash -ex
sudo apt update

 ##### ssh port change #####

sudo perl -pi -e 's/^#?Port 22$/Port 1337/' /etc/ssh/sshd_config

##### Config firewall #####

sudo ufw default deny incoming
sudo ufw default allow outgoing
sudo ufw allow 1337/tcp
sudo ufw allow from 10.0.10.0/24
sudo ufw enable
sudo ufw reload

##### Install and config user Docker #####

sudo apt-get -y install apt-transport-https ca-certificates curl software-properties-common
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu  $(lsb_release -cs)  stable" -y
sudo apt update
sudo apt-get install -y docker docker-compose maven git curl
sudo systemctl start docker
sudo systemctl enable docker
sudo groupadd docker
sudo usermod -aG docker ubuntu
sudo service sshd restart

##### install and build application #####

##### backend #####

cd jumia_phone_validator/
cd validator-backend/
sudo apt install maven -y
mvn clean install

##### frontend #####

cd ..
chown -R $USER ./validator-frontend
cd validator-frontend/
curl -o- https://raw.githubusercontent.com/nvm-sh/nvm/v0.34.0/install.sh | bash
. ~/.nvm/nvm.sh
nvm install --lts
rm -rf node_modules
npm install
npm run build

cd ..
sudo docker-compose up -d 

