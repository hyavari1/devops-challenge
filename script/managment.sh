 #!/bin/bash -ex
 
 ##### ssh port change #####

 sudo perl -pi -e 's/^#?Port 22$/Port 1337/' /etc/ssh/sshd_config
 
 ##### ansible #####
 
 sudo apt update
 sudo apt-get install apt-transport-https wget gnupg unzip -y
 sudo apt-add-repository ppa:ansible/ansible -y
 sudo apt-get update && sudo apt-get install ansible -y
 sudo apt  install awscli -y
 ##### terraform #####

 wget -O- https://apt.releases.hashicorp.com/gpg | gpg --dearmor | sudo tee /usr/share/keyrings/hashicorp-archive-keyring.gpg
 echo "deb [signed-by=/usr/share/keyrings/hashicorp-archive-keyring.gpg] https://apt.releases.hashicorp.com $(lsb_release -cs) main" | sudo tee /etc/apt/sources.list.d/hashicorp.list
 sudo apt update && sudo apt install terraform -y
 
 ##### AWS CLI #####
 
 curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
 unzip awscliv2.zip
 sudo ./aws/install
 
##### Config firewall #####
 
 sudo ufw default deny incoming
 sudo ufw default allow outgoing
 sudo ufw allow 1337/tcp
 sudo ufw allow from 10.0.0.0/16
 sudo ufw enable
 sudo ufw reload