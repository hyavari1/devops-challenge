# DevOps-Challenge
# Intro and My strategy
For this task, I will create infrastructure in AWS with Terraform after that for the configuration of the instance I got a hand from ansible to run the installation and other configs to the instance.
I create bash scripts for each instance and Nginx config file for the API load balancer 
also, create an ansible playbook to transfer scripts and execute them it uses an inventory file that uses terraform output to reach the instance (It would be automated but I had little time to do it) with SSH the other step will be automated like building and config application and Database and load balancer configuration.
I also create EKS deployment with Terraform and lunch the Nginx webserver with terraform manifest into k8s but I couldn't test it because I had enough time to do that because my Internet was down for two days
Set up and initialize your Terraform workspace

In your terminal, clone the following repository. It contains the example configuration used in this tutorial.
```
$git clone https://gitlab.com/hyavari1/devops-challenge
```

## Install and config Awscli with Terraform

AWS CLI config and insert secret and key or put in terraform.tfvars 

```
$aws configure
```
# Create Infrastructure
- run these commands:

```
$ terraform init
$ terraform validate
$ terraform fmt
$ terraform plan
$ terraform apply 
```
- For delete : 
```
$ terraform destroy
```
save output IP for config instance
# Terraform 
The Terraform code will create four Instances Microservice, Database, Loadbalancer, Management instance 

# Network
 ----------------
- # VPC and Subnet 
I use the current VPC that exists in the Jumia AWS account that's name is Devops-vpc with this subnet 10.0.0.0/16 because already you have five vpc and I can't create new ones.
I create three subnets that assign to each type of my services 
* Microservice Subnet 10.0.20.0/24
* Database Subnet 10.0.0.30.0/24
* Loadbalancer Subnet 10.0.10.0/24
* For management instance I use the Microservice subnet 
- # IGW (Internet Gataway)
like VPC I use existing IGW to route the instance to the internet 
- # Security Profile 
We have three type security profile for each instance I write comment in security.tf 

----------

# Instances



- # Microservice Instance 
The type of instance is t2-medium because we need more ram to run containers and build application code so we will install docker and docker-compose with maven and npm to run containers and build backend and frontend services they expose to port 8080 and 8081. This instance lunch in the Microservice subnet, The subnet is 10.0.20.0/24
- ## Config Database 
Postgress DB and other services installed by Terraform 
copy sample.sql and database script to database instance with ansible to copy and then run the database.sh to create a database user and password after that insert sample.sql 
- ## Troubleshoot Postgres for network access
Edit the below file to Postgres listen to any address and insert listen_addresses = '*'
```
$sudo vim /etc/postgresql/14/main/Postgresql.conf 
```
Edit the below file to Postgres to accept only the Microservice subnet 
```
$sudo vim /etc/postgresql/14/main/pg_hba.conf  
```
In this line you need to change the localhost IP to The service IP like this:
```
host    all             all             10.0.20.0/24          scram-sha-256
```
restart the Postgres database to apply changes 
```
#sudo systemctl restart Postgres
```
take note of privite database IP address
- ## Check database socket for listening port 
```
#netstat -anpt | grep LISTEN
```
- ## Config microservices 
For configuring the microservice you need to put the Database private IP in application.properties the other step has running by ansible with a run shell script like building frontend and backend also building Docker images with docker-compose and exposing them into their port.

- ## Config loadbalancer
I use Nginx Loadbalancer for load balance and API gateway to access other microservices easily 
with Loadbalancer IP you can access backend and frontend URLs without using a port and use URL to call them 

----------

## Ansible
For config Ansible inventory you need public IP with 22 port before the ansible-playbook run after that you have to define a 1337 ssh port to run other playbooks
for local access put private IP in Inventory.ini also for out-of-band management their infrastructure uses public IP 
```
$ansible-playbook -i ./script/inventory.ini -u ubuntu  ./script/ansible-playbook.yml
```
___
## Dashboard
With load balancer IP (public/private)you can call backend microservices with 
http://loadbalancerIP/api/v1/customers

For my test, it is running on this URL 

http://100.27.37.195/api/v1/customers

And for the frontend dashboard use a load balancer IP like below

http://loadbalancerIP

My test URL is :

http://100.27.37.195

----

## Cost of using AWS resource 
## Cost to EKS Infrastructure 

<img src="./Cost_Infrastructure_eks.png" />

## Cost to Infrastructure without EKS 

<img src="./Cost_Infrastructure_without_eks.png"/>
